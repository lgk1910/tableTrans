# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
from .architectures.detr import build
from .architectures.detr_multi import build as build_multi


def build_model(args):
    return build(args)

def build_model_multi(args):
    return build_multi(args)