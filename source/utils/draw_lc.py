
log_path = '/home/ec2-user/table_transformer_khoi/tableTrans/data/models/trained/det/20221122/log.txt'
with open(log_path, 'r') as f:
    log = f.read()

results = log.split('\n')
result_dicts = []
for res in results:
    try:
        result_dicts.append(eval(res))
    except Exception as E:
        print(f'Exception {E}')
        print(res)

print(result_dicts[0])