"""
Copyright (C) 2021 Microsoft Corporation
"""
import os
import json
from datetime import datetime
import string
import sys
import random
import numpy as np
import torch
from torchvision.transforms import functional as F
from infer.utility import get_args

def main():
    cmd_args = get_args().__dict__
    config_args = json.load(open(cmd_args['config_file'], 'rb'))
    for key, value in cmd_args.items():
        if not key in config_args or not value is None:
            config_args[key] = value
    #config_args.update(cmd_args)
    args = type('Args', (object,), config_args)
    print(args.__dict__)
    print('-' * 100)

    print(args.config_file)
    

if __name__ == "__main__":
    main()
