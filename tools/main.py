"""
Copyright (C) 2021 Microsoft Corporation
"""
import os
import json
from datetime import datetime
import string
import sys
import random
import numpy as np
import torch
from torchvision.transforms import functional as F
import cv2
import glob
from tqdm import tqdm

sys.path.append("../data")
sys.path.append("../source")
from engine import evaluate, train_one_epoch
import data.transforms as R

from eval import eval_coco

# New
from data.loader import get_data
from modeling.model import get_model
import utils.misc as utils
from utils.early_stopping import EarlyStopper
from infer.utility import get_args

def train(args, model, criterion, postprocessors, device):
    """
    Training loop
    """

    print("loading data")
    dataloading_time = datetime.now()
    data_loader_train, data_loader_val, dataset_val, train_len = get_data(args)
    print("finished loading data in :", datetime.now() - dataloading_time)

    model_without_ddp = model
    param_dicts = [
        {
            "params": [
                p for n, p in model_without_ddp.named_parameters()
                if "backbone" not in n and p.requires_grad
            ]
        },
        {
            "params": [
                p for n, p in model_without_ddp.named_parameters()
                if "backbone" in n and p.requires_grad
            ],
            "lr":
            args.lr_backbone,
        },
    ]
    optimizer = torch.optim.AdamW(param_dicts,
                                  lr=args.lr,
                                  weight_decay=args.weight_decay)

    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                   step_size=args.lr_drop,
                                                   gamma=args.lr_gamma)

    max_batches_per_epoch = int(train_len / args.batch_size)
    print("Max batches per epoch: {}".format(max_batches_per_epoch))

    resume_checkpoint = False
    if args.model_load_path:
        checkpoint = torch.load(args.model_load_path, map_location='cpu')
        if 'model_state_dict' in checkpoint:
            model.load_state_dict(checkpoint['model_state_dict'])

        model.to(device)

        if not args.load_weights_only and 'optimizer_state_dict' in checkpoint:
            optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            resume_checkpoint = True
        elif args.load_weights_only:
            print("*** WARNING: Resuming training and ignoring optimzer state. "
                  "Training will resume with new initialized values. "
                  "To use current optimizer state, remove the --load_weights_only flag.")
        else:
            print("*** ERROR: Optimizer state of saved checkpoint not found. "
                  "To resume training with new initialized values add the --load_weights_only flag.")
            raise Exception("ERROR: Optimizer state of saved checkpoint not found. Must add --load_weights_only flag to resume training without.")          
        
        if not args.load_weights_only and 'epoch' in checkpoint:
            args.start_epoch = checkpoint['epoch'] + 1
        elif args.load_weights_only:
            print("*** WARNING: Resuming training and ignoring previously saved epoch. "
                  "To resume from previously saved epoch, remove the --load_weights_only flag.")
        else:
            print("*** WARNING: Epoch of saved model not found. Starting at epoch {}.".format(args.start_epoch))

    # Use user-specified save directory, if specified
    if args.model_save_dir:
        output_directory = args.model_save_dir
    # If resuming from a checkpoint with optimizer state, save into same directory
    elif args.model_load_path and resume_checkpoint:
        output_directory = os.path.split(args.model_load_path)[0]
    # Create new save directory
    else:
        run_date = datetime.now().strftime("%Y%m%d%H%M%S")
        output_directory = os.path.join(args.data_root_dir, "output", run_date)

    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    print("Output directory: ", output_directory)
    model_save_path = os.path.join(output_directory, 'model.pth')
    print("Output model path: ", model_save_path)
    if not resume_checkpoint and os.path.exists(model_save_path):
        print("*** WARNING: Output model path exists but is not being used to resume training; training will overwrite it.")

    if args.start_epoch >= args.epochs:
        print("*** WARNING: Starting epoch ({}) is greater or equal to the number of training epochs ({}).".format(
            args.start_epoch, args.epochs
        ))

    print("Start training")
    print(f'Resume checkpoint: {resume_checkpoint}')
    start_time = datetime.now()
    early_stopper = EarlyStopper(patience=3, min_delta=0.005)
    for epoch in range(args.start_epoch, args.epochs):
        print('-' * 100)

        epoch_timing = datetime.now()
        train_stats = train_one_epoch(
            model,
            criterion,
            data_loader_train,
            optimizer,
            device,
            epoch,
            args.clip_max_norm,
            max_batches_per_epoch=max_batches_per_epoch,
            print_freq=1000)
        print("Epoch completed in ", datetime.now() - epoch_timing)

        lr_scheduler.step()

        pubmed_stats, coco_evaluator = evaluate(model, criterion,
                                                postprocessors,
                                                data_loader_val, dataset_val,
                                                device, None)
        val_loss = pubmed_stats['loss']
        print("pubmed: AP50: {:.3f}, AP75: {:.3f}, AP: {:.3f}, AR: {:.3f}".
              format(pubmed_stats['coco_eval_bbox'][1],
                     pubmed_stats['coco_eval_bbox'][2],
                     pubmed_stats['coco_eval_bbox'][0],
                     pubmed_stats['coco_eval_bbox'][8]))

        if early_stopper.early_stop(val_loss):
            print("Early Stopping!")          
            break
    
        # Save current model training progress
        torch.save({'epoch': epoch,
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    }, model_save_path)
        
        # Save checkpoint for evaluation
        if (epoch+1) % args.checkpoint_freq == 0:
            model_save_path_epoch = os.path.join(output_directory, 'model_' + str(epoch+1) + '.pth')
            torch.save(model.state_dict(), model_save_path_epoch)
        

    print('Total training time: ', datetime.now() - start_time)

def rescale_img(img, fraction=2):
    width = int(img.shape[1] * fraction)
    height = int(img.shape[0] * fraction)
    dim = (width, height)
    
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    return resized

def get_class_map():
    class_map = {
        0: "table",
        1: "table column",
        2: "table row",
        3: "table column header",
        4: "table projected row header",
        5: "table spanning cell",
        6: "no object",
    }
    return class_map

def infer(args, model, postprocessors, device):
    output_dir = args.output_dir
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        
    normalize = R.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    image_path = args.image_path
    image = cv2.imread(image_path)
    h, w, _ = image.shape
    print(f'w, h: {w} {h}')
    fraction = 1
    img = image
    if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
        fraction = 0.33
        img = rescale_img(image, fraction)
        print(f'new img shape {img.shape}')
    elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
        fraction = 0.2
        img = rescale_img(image, fraction)
        print(f'new img shape {img.shape}')
    elif (w >= 7000) or (h >= 7000):
        fraction = 0.15
        img = rescale_img(image, fraction)
        print(f'new img shape {img.shape}') 
    else:
        img = image
            
    img_tensor = normalize(F.to_tensor(img))[0]
    img_tensor = torch.unsqueeze(img_tensor, 0).to(device)

    outputs = None
    with torch.no_grad():
        outputs = model(img_tensor)

    image_size = torch.unsqueeze(torch.as_tensor([int(h), int(w)]), 0).to(device)
    results = postprocessors["bbox"](outputs, image_size)[0]
    print(results)
    class_map = get_class_map()
    ltrb_label = ''
    if args.debug is True:
        for idx, score in enumerate(results["scores"].tolist()):
            if score < args.thresh:
                continue

            xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
            ltrb_label += f'{class_map[results["labels"][idx].item()]} {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
            cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 255, 0), 2)
        results["debug_image"] = image
        with open(os.path.join(output_dir, 'output.txt'), 'w') as f:
            f.write(ltrb_label)
        cv2.imwrite(os.path.join(output_dir, 'output_image.jpg'), image)
    else:
        for idx, score in enumerate(results["scores"].tolist()):
            if score < args.thresh:
                continue
            xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
            ltrb_label += f'{class_map[results["labels"][idx].item()]} {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
        with open(os.path.join(output_dir, 'output.txt'), 'w') as f:
            f.write(ltrb_label)
    print(f'Infer successfully, output saved at {output_dir}')
    return ltrb_label, results


def infer_multi(args, model, postprocessors, device):
    output_dir = args.output_dir
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    if not os.path.exists(os.path.join(output_dir, 'txt')):
        os.makedirs(os.path.join(output_dir, 'txt'))
    
    if not os.path.exists(os.path.join(output_dir, 'debug_image')):
        os.makedirs(os.path.join(output_dir, 'debug_image'))
        
    normalize = R.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    image_paths = glob.glob(os.path.join(args.image_path, '*.jpg'))
    ress = []
    for img_path in tqdm(image_paths):
        basename = os.path.basename(img_path)
        img_name = '.'.join(basename.split('.')[:-1])
        if os.path.exists(os.path.join(output_dir, "txt", f"{img_name}.txt")):
            print(f'{img_name} processed')
            continue
        image = cv2.imread(img_path)
        h, w, _ = image.shape
        print(f'w, h: {w} {h}')
        fraction = 1
        img = image
        if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
            fraction = 0.33
            img = rescale_img(image, fraction)
            print(f'new img shape {img.shape}')
        elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
            fraction = 0.2
            img = rescale_img(image, fraction)
            print(f'new img shape {img.shape}')
        elif (w >= 7000) or (h >= 7000):
            fraction = 0.15
            img = rescale_img(image, fraction)
            print(f'new img shape {img.shape}') 
        else:
            img = image
                
        img_tensor = normalize(F.to_tensor(img))[0]
        img_tensor = torch.unsqueeze(img_tensor, 0).to(device)

        outputs = None
        with torch.no_grad():
            outputs = model(img_tensor)

        image_size = torch.unsqueeze(torch.as_tensor([int(h), int(w)]), 0).to(device)
        results = postprocessors["bbox"](outputs, image_size)[0]
        # print(results)
        ltrb_label = ''
        if args.debug is True:
            for idx, score in enumerate(results["scores"].tolist()):
                if score < args.thresh:
                    continue

                xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
                ltrb_label += f'table {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
                cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 255, 0), 2)
            results["debug_image"] = image
            with open(os.path.join(output_dir, 'txt', f"{img_name}.txt"), 'w') as f:
                f.write(ltrb_label)
            cv2.imwrite(os.path.join(output_dir, 'debug_image', f'{img_name}.jpg'), image)
        else:
            for idx, score in enumerate(results["scores"].tolist()):
                if score < args.thresh:
                    continue
                xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
                ltrb_label += f'table {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
            with open(os.path.join(output_dir, 'txt', f"{img_name}.txt"), 'w') as f:
                f.write(ltrb_label)
                
        ress.append(results)

    print(f'Infer successfully, output saved at {output_dir}')
    return ress

def main():
    cmd_args = get_args().__dict__
    config_args = json.load(open(cmd_args['config_file'], 'rb'))
    for key, value in cmd_args.items():
        if not key in config_args or not value is None:
            config_args[key] = value
    #config_args.update(cmd_args)
    args = type('Args', (object,), config_args)
    print(args.__dict__)
    print('-' * 100)

    # Check for debug mode
    if args.mode == 'eval' and args.debug:
        print("Running evaluation/inference in DEBUG mode, processing will take longer. Saving output to: {}.".format(args.debug_save_dir))
        os.makedirs(args.debug_save_dir, exist_ok=True)

    # fix the seed for reproducibility
    seed = args.seed + utils.get_rank()
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)

    print("loading model")
    device = torch.device(args.device)
    model, criterion, postprocessors = get_model(args, device)

    if args.mode == "train":
        train(args, model, criterion, postprocessors, device)
    elif args.mode == "eval":
        data_loader_test, dataset_test = get_data(args)
        eval_coco(args, model, criterion, postprocessors, data_loader_test, dataset_test, device)
    elif args.mode == "infer":
        if os.path.isdir(args.image_path):
            infer_multi(args, model, postprocessors, device)
        elif os.path.isfile(args.image_path):
            infer(args, model, postprocessors, device)
        else:
            raise FileNotFoundError(f"file or folder {args.image_path} not found")


if __name__ == "__main__":
    main()
