class Table_Detection:
    def __init__(self, checkpoint_path, config_path):
        # args = Args
        args = load_json(config_path)
        args = type("Args", (object,), args)

        assert os.path.exists(checkpoint_path), checkpoint_path
        print(args.__dict__)
        print(args)
        print("-" * 100)

        args.model_load_path = checkpoint_path

        # fix the seed for reproducibility
        seed = args.seed + utils.get_rank()
        torch.manual_seed(seed)
        np.random.seed(seed)
        random.seed(seed)

        print("loading model")
        self.device = torch.device(args.device)
        self.model, _, self.postprocessors = get_model(args, self.device)
        self.model.eval()

        class_map = get_class_map()

        self.normalize = R.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])


    def predict(self, image_path=None, debug=True, thresh=0.9, factor=1):
        if image_path is None:
            image_path = "/data/pubtables1m/PubTables1M-Structure-PASCAL-VOC/images/PMC514496_table_0.jpg"

        image = image_path
        if isinstance(image_path, str):
            image = Image.open(image_path).convert("RGB")
        
        w, h = image.size

        img_tensor = self.normalize(F.to_tensor(image))[0]
        img_tensor = torch.unsqueeze(img_tensor, 0).to(self.device)

        outputs = None
        with torch.no_grad():
            outputs = self.model(img_tensor)

        image_size = torch.unsqueeze(torch.as_tensor([int(h), int(w)]), 0).to(
            self.device
        )
        results = self.postprocessors["bbox"](outputs, image_size)[0]
        # print(results)
        ltrb_label = ''
        if debug is True:
          image = np.array(image)
          for idx, score in enumerate(results["scores"].tolist()):
              if score < thresh:
                  continue

              xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
              ltrb_label += f'table {score} {int(xmin/factor)} {int(ymin/factor)} {int(xmax/factor)} {int(ymax/factor)}\n'
              cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 255, 0), 2)
          results["debug_image"] = image
        return ltrb_label, results['scores'], results['labels'], results['boxes'], results["debug_image"]